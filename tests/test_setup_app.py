import os
import pytest
import bcrypt
from configparser import ConfigParser
from flask import Flask
from peewee import SqliteDatabase

from sebureem.setup_app import setup_app
from sebureem.models import Sebura, Sebuks

@pytest.fixture(scope='module')
def setup_app_fix():
    app = Flask(__name__)
    app.secret_key = "Sebureem test secret_key"
    app.register_blueprint(setup_app)
    yield app.test_client()

@pytest.fixture()
def setup_config_dir(tmpdir):
    if os.name == "posix":
        os.environ['XDG_CONFIG_HOME'] = str(tmpdir)
    elif os.name == "nt":
        os.environ['LOCALAPPDATA'] = str(tmpdir)
    yield tmpdir.join('sebureem')

@pytest.fixture()
def create_config_file(setup_config_dir):
    config_file = setup_config_dir.join('sebureem.ini')
    config_file.write("# Sebureem config file", ensure=True)
    yield config_file

@pytest.fixture()
def create_db(setup_config_dir):
    db_file = setup_config_dir.join('sebureem_test.db')
    print(db_file)
    db = SqliteDatabase(str(db_file))
    db.connect()
    db.create_tables([Sebura, Sebuks], safe=True)
    db.close()
    yield db_file

class TestSetupApp:

    def test_app_running(self, setup_app_fix):
        ret = setup_app_fix.get("/install/")
        assert b"Sebureem installer" in ret.data

    def test_set_config_file(self, setup_app_fix, setup_config_dir):
        config_file = setup_config_dir.join('sebureem.ini')
        ret = setup_app_fix.get("install/config/")
        assert b"Config file created" in ret.data
        assert "# Sebureem config file" in config_file.read()

    def test_create_config_file_exists(self, setup_app_fix, create_config_file):
        config_file = create_config_file
        assert "# Sebureem config file" in config_file.read()
        ret = setup_app_fix.get("install/config/")
        assert b"Warning: Config file found" in ret.data

    def test_create_config_dir_exists(self, setup_app_fix, setup_config_dir):
        config_dir = setup_config_dir
        config_dir.mkdir()
        ret = setup_app_fix.get("install/config/")
        config_file = setup_config_dir.join('sebureem.ini')
        assert b"Config file created" in ret.data
        assert "# Sebureem config file" in config_file.read()

    def test_set_db_path(self, setup_app_fix, tmpdir):
        ret = setup_app_fix.get("install/database/")
        assert b"Creating database" in ret.data

    def test_create_db(self, setup_app_fix, create_config_file, tmpdir):
        conf_path = create_config_file
        test_conf = ConfigParser(allow_no_value=True)
        db_path = tmpdir.join('sebureem')
        ret = setup_app_fix.post('/install/database/', data={
            'db_path' : str(db_path),
            'db_name': 'sebureem_test.db'
        })
        test_conf.read_file(conf_path.readlines())
        assert test_conf['DATABASE']['path'] == str(
           db_path.join('sebureem_test.db')
        )
        assert db_path.join('sebureem_test.db').check(file=True)
        assert b"Database created" in ret.data

    def test_create_db_exists(self, setup_app_fix, create_config_file, create_db, tmpdir):
        conf_path = create_config_file
        db_path = create_db
        print(str(db_path))
        test_conf = ConfigParser(allow_no_value=True)

        ret = setup_app_fix.post('/install/database/', data={
            'db_path' : str(db_path),
            'db_name': 'sebureem_test.db'
        })
        test_conf.read_file(conf_path.readlines())
        assert test_conf['DATABASE']['path'] == str(
           db_path.join('sebureem_test.db')
        )
        assert db_path.check(file=True)
        assert b"Database already present at this location" in ret.data

    def test_set_admin(self, setup_app_fix):
        ret = setup_app_fix.get("install/admin/")
        assert b"Admin account creation" in ret.data

    def test_create_admin(self, setup_app_fix, create_config_file):
        conf_path = create_config_file
        test_conf = ConfigParser(allow_no_value=True)
        ret = setup_app_fix.post('/install/admin/', data={
            'admin_login' : 'admin',
            'admin_passwd': 'admin'
        })
        test_conf.read_file(conf_path.readlines())
        assert test_conf['ADMIN']['login'] == 'admin'
        assert bcrypt.checkpw(b'admin', bytes(test_conf['ADMIN']['passwd'], 'utf-8')) 
        assert b"Admin account created" in ret.data

    def test_set_site(self, setup_app_fix):
        ret = setup_app_fix.get("install/site/")
        assert b"Creating site" in ret.data

    def test_create_site(self, setup_app_fix, create_config_file):
        conf_path = create_config_file
        test_conf = ConfigParser(allow_no_value=True)
        ret = setup_app_fix.post('/install/site/', data={
            'site_name' : 'foo',
            'site_url': 'https://exemple.com'
        })
        test_conf.read_file(conf_path.readlines())
        assert test_conf['foo']['url'] == 'https://exemple.com'
        assert b"Site configured" in ret.data
