#########
CHANGELOG
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <http://keepachangelog.com>`_
and this project adheres to `Semantic Versioning <http://semver.org/>`_.

Unreleased
==========

Added
-----

* Updated peewee dependancies

v0.2
====

Added
-----

* Console command ``> sebureem --init`` to start the setup web application
* Setup web application listening on ``http://localhost:8080/install``
* .gitlab.yml for Continuous Integration
* Tests (with `pytest <https://docs.pytest.org/en/latest/>`_)

v0.1
====

Added
-----

* Badly designed comment server